import 'package:mobx/mobx.dart';
part 'data.g.dart';

class Data = _Data with _$Data;

abstract class _Data with Store {
  @observable
  var results = [];
  @observable
  List<Map<String, dynamic>> movies = [
    {
      'id': 0,
      'title': 'Power Ranger',
      'director': 'Dean Israelite',
      'genres': ['Action', 'Sci-Fi'],
      'summary':
          'Group of superheroes that using a costume with a different colors and fighting against monsters'
    },
    {
      'id': 1,
      'title': 'Frozen',
      'director': 'Chris Buck',
      'genres': ['Animation'],
      'summary': 'Couple of sisters in a snow kingdom'
    },
  ];

  @observable
  List<Map<String, dynamic>> genres = [
    {
      'g': 'Drama',
      'value': true,
    },
    {
      'g': 'Action',
      'value': false,
    },
    {
      'g': 'Animation',
      'value': false,
    },
    {
      'g': 'Sci-Fi',
      'value': false,
    },
    {
      'g': 'Horror',
      'value': false,
    }
  ];

  @action
  void init() {
    results = movies;
  }

  @action
  void search(input) {
    if (input.isEmpty) {
      results = movies;
    } else {
      results = movies
          .where((e) => e['title']
              .toString()
              .toLowerCase()
              .contains(input.toString().toLowerCase()))
          .toList();
    }
  }

  @action
  void create(title, director, summary, genre) {
    results.add({
      'id': movies.length,
      'title': title,
      'director': director,
      'summary': summary,
      'genre': genre
    });
    init();
  }

  @action
  void delete(id) {
    movies.removeWhere((movie) => movie['id'] == id);
    results = movies;
  }

  @action
  void update(id, title, director, summary, genre) {
    for (int i = 0; i < results.length; i++) {
      if (id == results[i]['id']) {
        results[i]['title'] = title;
        results[i]['director'] = director;
        results[i]['summary'] = summary;
        // results[i]['genres'] = genre;
      }
    }
    init();
  }

  @action
  void assignGenre(i, a) {
    genres[i]['value'] = a;
  }
}
