// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Data on _Data, Store {
  late final _$resultsAtom = Atom(name: '_Data.results', context: context);

  @override
  List<dynamic> get results {
    _$resultsAtom.reportRead();
    return super.results;
  }

  @override
  set results(List<dynamic> value) {
    _$resultsAtom.reportWrite(value, super.results, () {
      super.results = value;
    });
  }

  late final _$moviesAtom = Atom(name: '_Data.movies', context: context);

  @override
  List<Map<String, dynamic>> get movies {
    _$moviesAtom.reportRead();
    return super.movies;
  }

  @override
  set movies(List<Map<String, dynamic>> value) {
    _$moviesAtom.reportWrite(value, super.movies, () {
      super.movies = value;
    });
  }

  late final _$genresAtom = Atom(name: '_Data.genres', context: context);

  @override
  List<Map<String, dynamic>> get genres {
    _$genresAtom.reportRead();
    return super.genres;
  }

  @override
  set genres(List<Map<String, dynamic>> value) {
    _$genresAtom.reportWrite(value, super.genres, () {
      super.genres = value;
    });
  }

  late final _$_DataActionController =
      ActionController(name: '_Data', context: context);

  @override
  void init() {
    final _$actionInfo =
        _$_DataActionController.startAction(name: '_Data.init');
    try {
      return super.init();
    } finally {
      _$_DataActionController.endAction(_$actionInfo);
    }
  }

  @override
  void search(dynamic input) {
    final _$actionInfo =
        _$_DataActionController.startAction(name: '_Data.search');
    try {
      return super.search(input);
    } finally {
      _$_DataActionController.endAction(_$actionInfo);
    }
  }

  @override
  void create(dynamic title, dynamic director, dynamic summary, dynamic genre) {
    final _$actionInfo =
        _$_DataActionController.startAction(name: '_Data.create');
    try {
      return super.create(title, director, summary, genre);
    } finally {
      _$_DataActionController.endAction(_$actionInfo);
    }
  }

  @override
  void delete(dynamic id) {
    final _$actionInfo =
        _$_DataActionController.startAction(name: '_Data.delete');
    try {
      return super.delete(id);
    } finally {
      _$_DataActionController.endAction(_$actionInfo);
    }
  }

  @override
  void update(dynamic id, dynamic title, dynamic director, dynamic summary,
      dynamic genre) {
    final _$actionInfo =
        _$_DataActionController.startAction(name: '_Data.update');
    try {
      return super.update(id, title, director, summary, genre);
    } finally {
      _$_DataActionController.endAction(_$actionInfo);
    }
  }

  @override
  void assignGenre(dynamic a, dynamic b) {
    final _$actionInfo =
        _$_DataActionController.startAction(name: '_Data.assignGenre');
    try {
      return super.assignGenre(a, b);
    } finally {
      _$_DataActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
results: ${results},
movies: ${movies},
genres: ${genres}
    ''';
  }
}
