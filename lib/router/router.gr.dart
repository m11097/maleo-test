// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

import 'package:auto_route/auto_route.dart' as _i5;
import 'package:flutter/material.dart' as _i6;
import 'package:maleo_test/view/add_screen.dart' as _i3;
import 'package:maleo_test/view/detail_screen.dart' as _i2;
import 'package:maleo_test/view/edit_screen.dart' as _i4;
import 'package:maleo_test/view/home_screen.dart' as _i1;

class AppRouter extends _i5.RootStackRouter {
  AppRouter([_i6.GlobalKey<_i6.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i5.PageFactory> pagesMap = {
    HomeScreenRoute.name: (routeData) {
      return _i5.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i1.HomeScreen());
    },
    DetailScreenRoute.name: (routeData) {
      final args = routeData.argsAs<DetailScreenRouteArgs>();
      return _i5.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i2.DetailScreen(
              key: args.key,
              id: args.id,
              title: args.title,
              director: args.director,
              genres: args.genres,
              summary: args.summary));
    },
    AddScreenRoute.name: (routeData) {
      final args = routeData.argsAs<AddScreenRouteArgs>(
          orElse: () => const AddScreenRouteArgs());
      return _i5.AdaptivePage<dynamic>(
          routeData: routeData, child: _i3.AddScreen(key: args.key));
    },
    EditScreenRoute.name: (routeData) {
      final args = routeData.argsAs<EditScreenRouteArgs>();
      return _i5.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i4.EditScreen(
              key: args.key,
              id: args.id,
              title: args.title,
              director: args.director,
              genres: args.genres,
              summary: args.summary));
    }
  };

  @override
  List<_i5.RouteConfig> get routes => [
        _i5.RouteConfig(HomeScreenRoute.name, path: '/'),
        _i5.RouteConfig(DetailScreenRoute.name, path: '/detail-screen'),
        _i5.RouteConfig(AddScreenRoute.name, path: '/add-screen'),
        _i5.RouteConfig(EditScreenRoute.name, path: '/edit-screen')
      ];
}

/// generated route for
/// [_i1.HomeScreen]
class HomeScreenRoute extends _i5.PageRouteInfo<void> {
  const HomeScreenRoute() : super(HomeScreenRoute.name, path: '/');

  static const String name = 'HomeScreenRoute';
}

/// generated route for
/// [_i2.DetailScreen]
class DetailScreenRoute extends _i5.PageRouteInfo<DetailScreenRouteArgs> {
  DetailScreenRoute(
      {_i6.Key? key,
      required int? id,
      required String? title,
      required String? director,
      required List<dynamic> genres,
      required String? summary})
      : super(DetailScreenRoute.name,
            path: '/detail-screen',
            args: DetailScreenRouteArgs(
                key: key,
                id: id,
                title: title,
                director: director,
                genres: genres,
                summary: summary));

  static const String name = 'DetailScreenRoute';
}

class DetailScreenRouteArgs {
  const DetailScreenRouteArgs(
      {this.key,
      required this.id,
      required this.title,
      required this.director,
      required this.genres,
      required this.summary});

  final _i6.Key? key;

  final int? id;

  final String? title;

  final String? director;

  final List<dynamic> genres;

  final String? summary;

  @override
  String toString() {
    return 'DetailScreenRouteArgs{key: $key, id: $id, title: $title, director: $director, genres: $genres, summary: $summary}';
  }
}

/// generated route for
/// [_i3.AddScreen]
class AddScreenRoute extends _i5.PageRouteInfo<AddScreenRouteArgs> {
  AddScreenRoute({_i6.Key? key})
      : super(AddScreenRoute.name,
            path: '/add-screen', args: AddScreenRouteArgs(key: key));

  static const String name = 'AddScreenRoute';
}

class AddScreenRouteArgs {
  const AddScreenRouteArgs({this.key});

  final _i6.Key? key;

  @override
  String toString() {
    return 'AddScreenRouteArgs{key: $key}';
  }
}

/// generated route for
/// [_i4.EditScreen]
class EditScreenRoute extends _i5.PageRouteInfo<EditScreenRouteArgs> {
  EditScreenRoute(
      {_i6.Key? key,
      required int? id,
      required String? title,
      required String? director,
      required List<dynamic> genres,
      required String? summary})
      : super(EditScreenRoute.name,
            path: '/edit-screen',
            args: EditScreenRouteArgs(
                key: key,
                id: id,
                title: title,
                director: director,
                genres: genres,
                summary: summary));

  static const String name = 'EditScreenRoute';
}

class EditScreenRouteArgs {
  const EditScreenRouteArgs(
      {this.key,
      required this.id,
      required this.title,
      required this.director,
      required this.genres,
      required this.summary});

  final _i6.Key? key;

  final int? id;

  final String? title;

  final String? director;

  final List<dynamic> genres;

  final String? summary;

  @override
  String toString() {
    return 'EditScreenRouteArgs{key: $key, id: $id, title: $title, director: $director, genres: $genres, summary: $summary}';
  }
}
