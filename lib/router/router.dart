import 'package:auto_route/auto_route.dart';
import 'package:maleo_test/view/add_screen.dart';
import 'package:maleo_test/view/home_screen.dart';
import 'package:maleo_test/view/edit_screen.dart';

import '../view/detail_screen.dart';

@AdaptiveAutoRouter(routes: <AutoRoute>[
  AutoRoute(page: HomeScreen, initial: true),
  AutoRoute(page: DetailScreen),
  AutoRoute(page: AddScreen),
  AutoRoute(page: EditScreen)
])
class $AppRouter {}
