import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:maleo_test/view/home_screen.dart';

class AddScreen extends StatelessWidget {
  AddScreen({Key? key}) : super(key: key);
  //
  final TextEditingController _titleC = TextEditingController();
  final TextEditingController _directorC = TextEditingController();
  final TextEditingController _summaryC = TextEditingController();
  // final TextEditingController _genreC = TextEditingController();
  Widget titleWidget(width) {
    return Column(
      children: [
        const Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "Title",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          width: width,
          height: 40,
          padding: const EdgeInsets.all(10),
          margin: const EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(15),
          ),
          child: TextFormField(
            cursorColor: Colors.grey,
            decoration: const InputDecoration(
              hintText: 'title',
              border: InputBorder.none,
            ),
            controller: _titleC,
          ),
        ),
      ],
    );
  }

  Widget directorWidget(width) {
    return Column(
      children: [
        const Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "Director",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          width: width,
          height: 40,
          padding: const EdgeInsets.all(10),
          margin: const EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(15),
          ),
          child: TextFormField(
            cursorColor: Colors.grey,
            decoration: const InputDecoration(
              hintText: 'director',
              border: InputBorder.none,
            ),
            controller: _directorC,
          ),
        )
      ],
    );
  }

  Widget summaryWidget(width) {
    return Column(
      children: [
        const Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "Summary",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          width: width,
          height: 70,
          padding: const EdgeInsets.all(10),
          margin: const EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(15),
          ),
          child: TextFormField(
            cursorColor: Colors.grey,
            decoration: const InputDecoration(
              hintText: 'summary',
              border: InputBorder.none,
            ),
            controller: _summaryC,
          ),
        ),
      ],
    );
  }

  Widget genreWidget(width) {
    return Column(
      children: [
        const Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "Genre",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ),
        Container(
            width: width,
            height: 270,
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.only(bottom: 10),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(15),
            ),
            child: Observer(
              builder: (context) => ListView.builder(
                  itemCount: data.genres.length,
                  itemBuilder: ((context, index) {
                    return CheckboxListTile(
                        title: Text(
                          data.genres[index]['g'],
                          style: const TextStyle(fontSize: 14),
                        ),
                        value: data.genres[index]['value'],
                        onChanged: (val) {
                          data.assignGenre(index, val!);
                        });
                  })),
            ))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Add Movie"),
        actions: <Widget>[
          TextButton(
              onPressed: () {
                data.create(
                    _titleC.text, _directorC.text, _summaryC.text, data.genres);
                context.router.popUntilRoot();
              },
              child: const Icon(
                Icons.save,
                color: Colors.white,
                size: 30,
              )),
        ],
        backgroundColor: Colors.grey,
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            //TITTLE
            titleWidget(width),
            //DIRECTOR
            directorWidget(width),
            //SUMMARY
            summaryWidget(width),
            //GENRES
            genreWidget(width),
          ],
        ),
      ),
    );
  }
}
