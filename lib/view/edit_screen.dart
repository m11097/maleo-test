import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:maleo_test/view/home_screen.dart';

class EditScreen extends StatelessWidget {
  EditScreen({
    Key? key,
    required this.id,
    required this.title,
    required this.director,
    required this.genres,
    required this.summary,
  }) : super(key: key);
  final String? title, director, summary;
  final int? id;
  final List genres;

  final TextEditingController _titleC = TextEditingController();
  final TextEditingController _directorC = TextEditingController();
  final TextEditingController _summaryC = TextEditingController();

  Widget titleWidget(width) {
    _titleC.text = title!;
    return Column(
      children: [
        const Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "Title",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          width: width,
          height: 40,
          padding: const EdgeInsets.all(10),
          margin: const EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(15),
          ),
          child: TextFormField(
            controller: _titleC,
            style: const TextStyle(fontSize: 14),
            decoration: const InputDecoration(
              border: InputBorder.none,
            ),
          ),
        ),
      ],
    );
  }

  Widget directorWidget(width) {
    _directorC.text = director!;
    return Column(
      children: [
        const Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "Director",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          width: width,
          height: 40,
          padding: const EdgeInsets.all(10),
          margin: const EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(15),
          ),
          child: TextFormField(
            controller: _directorC,
            style: const TextStyle(fontSize: 14),
            decoration: const InputDecoration(
              border: InputBorder.none,
            ),
          ),
        )
      ],
    );
  }

  Widget summaryWidget(width) {
    _summaryC.text = summary!;
    return Column(
      children: [
        const Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "Summary",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          width: width,
          height: 100,
          padding: const EdgeInsets.all(10),
          margin: const EdgeInsets.only(bottom: 20),
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(15),
          ),
          child: TextFormField(
            controller: _summaryC,
            style: const TextStyle(fontSize: 14),
            decoration: const InputDecoration(
              border: InputBorder.none,
            ),
          ),
        ),
      ],
    );
  }

  Widget genreWidget(width) {
    return Column(
      children: [
        const Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "Genre",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ),
        Container(
            width: width,
            height: 260,
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.only(bottom: 10),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(15),
            ),
            child: Observer(
              builder: (context) => ListView.builder(
                  itemCount: data.genres.length,
                  itemBuilder: ((context, index) {
                    return CheckboxListTile(
                        title: Text(
                          data.genres[index]['g'],
                          style: const TextStyle(fontSize: 14),
                        ),
                        value: data.genres[index]['value'],
                        onChanged: (val) {
                          data.assignGenre(index, val!);
                        });
                  })),
            ))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    // titleC.text = title!;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Edit Movie"),
        actions: <Widget>[
          TextButton(
              onPressed: () {
                data.update(
                    id, _titleC.text, _directorC.text, _summaryC.text, genres);
                context.router.popUntilRoot();
              },
              child: const Icon(
                Icons.save,
                color: Colors.white,
                size: 30,
              )),
        ],
        backgroundColor: Colors.grey,
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            //TITTLE
            titleWidget(width),
            //DIRECTOR
            directorWidget(width),
            //SUMMARY
            summaryWidget(width),
            //GENRES
            genreWidget(width),
          ],
        ),
      ),
    );
  }
}
