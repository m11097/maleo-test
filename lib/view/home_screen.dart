import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:maleo_test/mobx/data.dart';
import 'package:maleo_test/router/router.gr.dart';

final Data data = Data();

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  Widget searchbar(width) {
    return Container(
      width: width,
      height: 40,
      margin: const EdgeInsets.only(bottom: 20),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 1,
              blurRadius: 20,
            )
          ]),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const Icon(
            Icons.search,
            size: 20,
            color: Colors.grey,
          ),
          Expanded(
            child: TextFormField(
              cursorColor: Colors.grey,
              decoration: const InputDecoration(
                hintText: 'Search',
                border: InputBorder.none,
              ),
              onChanged: (value) => data.search(value),
            ),
          ),
        ],
      ),
    );
  }

  Widget lists(width) {
    return Expanded(
      child: Observer(
          builder: (context) => data.results.isNotEmpty
              ? ListView.builder(
                  itemCount: data.results.length,
                  itemBuilder: ((context, index) {
                    return TextButton(
                      onPressed: (() {
                        context.router.push(DetailScreenRoute(
                            id: data.results[index]['id'],
                            title: data.results[index]['title'],
                            director: data.results[index]['director'],
                            genres: data.results[index]['genres'],
                            summary: data.results[index]['summary']));
                      }),
                      style: ButtonStyle(
                          overlayColor: MaterialStateProperty.resolveWith(
                              (states) => Colors.transparent)),
                      child: Container(
                        width: width,
                        height: 100,
                        padding: const EdgeInsets.all(10),
                        margin: const EdgeInsets.only(bottom: 10),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                spreadRadius: 1,
                                blurRadius: 20,
                              )
                            ]),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                              data.results[index]['title'].toString(),
                              style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Colors.black),
                            ),
                            Text(
                              data.results[index]['director'].toString(),
                              style: const TextStyle(
                                  fontSize: 14, color: Colors.black),
                            ),
                            Text(
                              data.results[index]['genres'].toString(),
                              style: const TextStyle(
                                  fontSize: 14, color: Colors.black),
                            )
                          ],
                        ),
                      ),
                    );
                  }))
              : const Center(
                  child: Text(
                    'No Results Found',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                )),
    );
  }

  Widget addbutton(context) {
    return Align(
        alignment: Alignment.centerRight,
        child: ElevatedButton(
            onPressed: () {
              AutoRouter.of(context).pushNamed('/add-screen');
            },
            style: ElevatedButton.styleFrom(
              primary: Colors.grey,
            ),
            child: const Icon(
              Icons.add,
              size: 30,
            )));
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    data.init();
    return Scaffold(
      appBar: AppBar(
        title: const Text("Movies Collection"),
        centerTitle: true,
        backgroundColor: Colors.grey,
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            //SEARCHBAR
            searchbar(width),
            //LISTS
            lists(width),
            //BLANK SPACE
            const SizedBox(
              height: 10,
            ),
            //ADD BUTTON
            addbutton(context),
          ],
        ),
      ),
    );
  }
}
