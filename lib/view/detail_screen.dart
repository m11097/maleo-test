import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:maleo_test/router/router.gr.dart';
import 'package:maleo_test/view/home_screen.dart';

class DetailScreen extends StatelessWidget {
  const DetailScreen({
    Key? key,
    required this.id,
    required this.title,
    required this.director,
    required this.genres,
    required this.summary,
  }) : super(key: key);
  final String? title, director, summary;
  final int? id;
  final List genres;
  Widget titleWidget(width) {
    return Column(
      children: [
        const Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "Title",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          width: width,
          height: 40,
          padding: const EdgeInsets.all(10),
          margin: const EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(15),
          ),
          child: Text(
            title!,
            style: const TextStyle(fontSize: 14),
          ),
        ),
      ],
    );
  }

  Widget directorWidget(width) {
    return Column(
      children: [
        const Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "Director",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          width: width,
          height: 40,
          padding: const EdgeInsets.all(10),
          margin: const EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(15),
          ),
          child: Text(
            director!,
            style: const TextStyle(fontSize: 14),
          ),
        )
      ],
    );
  }

  Widget summaryWidget(width) {
    return Column(
      children: [
        const Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "Summary",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          width: width,
          height: 100,
          padding: const EdgeInsets.all(10),
          margin: const EdgeInsets.only(bottom: 20),
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(15),
          ),
          child: Text(
            summary!,
            style: const TextStyle(fontSize: 14),
          ),
        ),
      ],
    );
  }

  Widget genresWidget() {
    return Expanded(
        child: GridView.builder(
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 110,
              mainAxisExtent: 40,
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
            ),
            itemCount: genres.length,
            itemBuilder: (BuildContext context, index) {
              return Center(
                child: Container(
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(color: Colors.grey)),
                  child: Text(genres[index].toString()),
                ),
              );
            }));
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Movie Detail"),
        actions: <Widget>[
          TextButton(
              onPressed: () {
                data.delete(id);
                context.router.pop<bool>(true);
              },
              child: const Icon(
                Icons.delete,
                color: Colors.red,
                size: 30,
              )),
          TextButton(
              onPressed: () {
                context.router.push(EditScreenRoute(
                    id: id,
                    title: title,
                    director: director,
                    summary: summary,
                    genres: genres));
              },
              child: const Icon(
                Icons.edit,
                color: Colors.white,
                size: 30,
              )),
        ],
        backgroundColor: Colors.grey,
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            //TITTLE
            titleWidget(width),
            //DIRECTOR
            directorWidget(width),
            //SUMMARY
            summaryWidget(width),
            //GENRES
            genresWidget(),
          ],
        ),
      ),
    );
  }
}
